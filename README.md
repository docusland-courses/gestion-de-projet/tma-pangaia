# Cahier des charges : Pangaia 
![Diagramme de classe](./diagramme-user.jpg)

## Objectif
A partir du cahier des charges suivants, définir un plan de test.
Vous pouvez respecter le format que vous souhaitez, en vrai je vous recommande un fichier XLS ou une plateforme telle que squash.
Listez l'ensemble des exigences et définisser un plan de test correspondant au cahier des charges présenté ci-dessous.


## Compte Demo
Un utilisateur peut essayer l'application. Un email, mot de passe et nom d'utilisateur sont automatiquement générés. Un jardin également.

L'email automatiquement généré aura le format suivant :

-   [texte-unique-aleatoire@demo-pangaia.fr](mailto:texte-unique-aleatoire@demo-pangaia.fr)

Après avoir cliqué sur le bouton essayer, l'utilisateur voit une modale affichant : `Un compte demo a été généré pour vous, vous permettant d'essayer l'application pangaia. Attention ce compte sera automatiquement supprimé après 24 heures. Il vous est possible à tout moment de sauvegarder ce compte en modifiant votre profil et en saisissant votre propre adresse email.`

L'utilisateur peut ensuite accéder à son profil et modifier son email. S'il le fait son compte devient un compte utilisateur classique.

## Inscription utilisateur
Un utilisateur peut s'inscrire avec un email et un mot de passe. Il doit saisir deux fois le mot de passe. Lors de son inscription, l'utilisateur indique son niveau en jardinage. Il peut également saisir un code postal lié à sa localité. Un message explicite lui indique que cette donnée ne sera utilisée que pour gérer les informations météorologiques et pour intégrer la gestion du climat.

-    Si l'email est déjà utilisé il ne peut s'inscrire mais un message lui indique que l'email est déjà utilisé
-    Si l'email ne correspond pas à un format d'email une erreur est affichée
-   Suite à l'inscription, un email de validation est envoyé à l'utilisateur
-    Si les deux champs mots de passe ne sont pas identiques une erreur est affichée.
-    Un générateur automatique de mot de passe doit être disponible
-    Si le mot de passe ne respecte pas les contraintes imposées, un message doit être affiché
-    Le code postal est optionnel
-    Le choix niveau de jardinage novice est sélectionné par défaut

Une fois inscrit, l'utilisateur est redirigé vers la page de connection.

Recommandation CNIL 2023 choisie pour la génération d'un mot de passe :

-   une phrase de passe doit être utilisée et elle doit être composée d’au minimum 7 mots.

## Compte utilisateur

Un utilisateur peut se connecter au sein de l'application. Il doit saisir son email et son mot de passe.

-    Si l'email et/ou le mot de passe sont erronés, un message lui indique que les identifiants sont erronés.
-    Si l'email ne correspond pas à un format d'email une erreur est affichée

Après trois tentatives erronées, le compte utilisateur est locké, un email est envoyé à l'adresse mail de l'utilisateur ciblé avec un lien permettant de renouveller son mot de passe.

Une fois connecté, si l'utilisateur ne dispose que d'un jardin, il est redirigé vers la page `homepage`. Une fois connecté, si l'utilisateur dispose de plusieurs jardins, il est redirigé vers la page `choose-garden` et doit choisir un jardin. Une fois un jardin choisi, l'utilisateur est redirigé vers `homepage`.

Finalement, lorsqu'un utilisateur visualise un jardin, il a la possibilité de partager son jardin avec un autre utilisateur.
